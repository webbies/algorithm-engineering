// #define PAPI
// http://icl.cs.utk.edu/projects/papi/wiki/PAPI3:PAPI_presets.3
#ifdef PAPI
#include <papi.h>
#endif
#include <iostream>
#include <string.h>
#include <assert.h>
#include <algorithm>
#include <iomanip>
#include <stdint-gcc.h>
#include <array>


// Prefixed by L1, L2 and L3:
// DCM = Data Cache Misses
// ICM = Instruction Cache Misses
// TCM = Total Cache Misses

// Prefixed by L1 and L2:
// LDM = load misses
// STM = store misses

// Prefixed by TLB
// DM = Data Translation
// IM = Instruction Translation


#define NUMBER_OF_EVENTS 1
#define EVENTS {PAPI_L2_DCM }

typedef int(*QueryFunction)(void);
typedef void(*InitializeFunction)(void);
struct Test {
    Test(std::string name, QueryFunction query, InitializeFunction initialize) : name(name), query(query), initialize(initialize) { }
    std::string name;
    QueryFunction query;
    InitializeFunction initialize;
};

// Getting comma as decimal seperator, thanks to: http://stackoverflow.com/questions/1422151/how-to-print-a-double-with-a-comma
template<typename CharT> class DecimalSeparator : public std::numpunct<CharT> {
public:
    DecimalSeparator(CharT Separator) : m_Separator(Separator) {}
    CharT do_decimal_point()const {return m_Separator;}
    CharT m_Separator;
};

double test(Test test);

#define REPS 1
int N;

int sum = 0;

typedef double element_t;

struct MatrixView {
    size_t base_row;
    size_t base_column;
    size_t rows;
    size_t columns;
};

element_t& lookup(size_t i, size_t j, element_t* matrix, size_t rows, size_t columns, const MatrixView& view) {
    assert(i < view.rows);
    assert(j < view.columns);
    return matrix[(view.base_row + i) * columns + (view.base_column + j)];
}

class MatrixData {
    size_t _rows, _columns;
    element_t* _elements;
public:
    MatrixData(int rows, int columns, element_t* elements) : _rows(rows), _columns(columns), _elements(elements) {}
    ~MatrixData() {}

    void free() {
        ::free(_elements);
    }

    void zero() {
        memset(_elements, 0, sizeof(element_t) * _rows * _columns);
    }

    element_t& operator()(size_t i, size_t j) {
        return _elements[i * _columns + j];
    }

    const element_t& operator()(size_t i, size_t j) const {
        return _elements[i * _columns + j];
    }

    MatrixView view() const {
        return {0, 0, _rows, _columns};
    }

//    const size_t& rows() const { return _rows; }
//    const size_t& columns() const { return _columns; }
};

class Matrix {
    MatrixData _data;
    MatrixView _view;

public:
    Matrix(const MatrixData& data, const MatrixView& view) : _data(data), _view(view) {}

    void free() {
        _data.free();
    }

    element_t& operator()(size_t i, size_t j) {
        return _data(_view.base_row + i, _view.base_column + j);
    }

    const element_t& operator()(size_t i, size_t j) const {
        return _data(_view.base_row + i, _view.base_column + j);
    }

    MatrixView subview(size_t base_row, size_t base_column, size_t rows, size_t columns) const {
        assert(base_row + rows <= _view.rows);
        assert(base_column + columns <= _view.columns);
        return {_view.base_row + base_row, _view.base_column + base_column, rows, columns};
    }

    const MatrixData& data() const { return _data; }

    const size_t& rows() const { return _view.rows; }
    const size_t& columns() const { return _view.columns; }
};


// Something must be allocated in the initializefunction
Matrix *A;
Matrix *B;
Matrix *C;


void rec_mult(const Matrix& A, const Matrix& B, Matrix& C) {
    assert(A.columns() == B.rows());
    assert(C.rows() == A.rows() && C.columns() == B.columns());

    size_t m = A.rows();
    size_t n = A.columns();
    size_t p = B.columns();

    if(m == 1 && n == 1 && p == 1) {
        // Base case, simple scalar multiplication.
        C(0, 0) += A(0, 0) * B(0, 0);
    } else if(m >= std::max(n, p)) {
        // Create two new matrices A1 and A2 with each half of the rows of A.
        size_t split = m / 2;

        const Matrix A1(A.data(), A.subview(0, 0, split, A.columns()));
        const Matrix A2(A.data(), A.subview(split, 0, m - split, A.columns()));

        Matrix C1(C.data(), C.subview(0, 0, split, C.columns()));
        Matrix C2(C.data(), C.subview(split, 0, m - split, C.columns()));

        rec_mult(A1, B, C1);
        rec_mult(A2, B, C2);
    } else if(n >= std::max(m, p)) {
        // Create two new matrices A1 and A2 with each half of the columns of A.
        size_t split = n / 2;

        const Matrix A1(A.data(), A.subview(0, 0, A.rows(), split));
        const Matrix A2(A.data(), A.subview(0, split, A.rows(), n - split));

        // Create two new matrices B1 and B2 with each half of the rows of B.
        const Matrix B1(B.data(), B.subview(0, 0, split, B.columns()));
        const Matrix B2(B.data(), B.subview(split, 0, n - split, B.columns()));

        rec_mult(A1, B1, C);
        rec_mult(A2, B2, C);
    } else if(p >= std::max(m, n)) {
        size_t split = p / 2;

        const Matrix B1(B.data(), B.subview(0, 0, B.rows(), split));
        const Matrix B2(B.data(), B.subview(0, split, B.rows(), p - split));

        Matrix C1(C.data(), C.subview(0, 0, C.rows(), split));
        Matrix C2(C.data(), C.subview(0, split, C.rows(), p - split));

        rec_mult(A, B1, C1);
        rec_mult(A, B2, C2);
    } else {
        assert(0);
    }
}

// A is an m x n matrix
// B is an n x p matrix
// C is an m x p matrix

void setRandomValues(Matrix& mat) {
    for(int i = 0; i < mat.rows(); i++) {
        for (int j = 0; j < mat.columns(); j++) {
            mat(i, j) = element_t((rand() % 1024)) / 1024.0;
        }
    }
}

void print(const Matrix& mat) {
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::fixed << std::setprecision(10);
    for(int i = 0; i < mat.rows(); i++) {
        std::cout << "[";
        for(int j = 0; j < mat.columns() - 1; j++) {
            std::cout << mat(i, j) << ",\t\t";
        }
        std::cout << mat(i, mat.columns() - 1) << "]" << std::endl;
    }
}

void mult(const Matrix& A, const Matrix& B, Matrix& C) {
    assert(A.columns() == B.rows());
    assert(C.rows() == A.rows() && C.columns() == B.columns());

    for(size_t row = 0; row < C.rows(); row++) {
        for(size_t col = 0; col < C.columns(); col++) {
            element_t inner_product(0.0);
            for(size_t k = 0; k < A.columns(); k++) {
                inner_product += A(row, k) * B(k, col);
            }
            C(row, col) = inner_product;
        }
    }
}

void transpose(const Matrix& from, Matrix&into) {
    assert(from.columns() == into.rows());
    assert(from.rows() == into.columns());

    for (size_t row = 0; row < from.rows(); row++) {
        for (size_t col = 0; col < from.columns(); col++) {
            into(col, row) = from(row, col);
        }
    }
}

void transposeMult(const Matrix& A, const Matrix& B, Matrix& C) {
    assert(A.columns() == B.rows());
    assert(C.rows() == A.rows() && C.columns() == B.columns());

    MatrixData B_data(B.columns(), B.rows(), new element_t[B.columns() * B.rows()]);
    Matrix newB(B_data, B_data.view());
    transpose(B, newB);

    for(size_t row = 0; row < C.rows(); row++) {
        for(size_t col = 0; col < C.columns(); col++) {
            element_t inner_product(0.0);
            for(size_t k = 0; k < A.columns(); k++) {
                inner_product += A(row, k) * newB(col, k);
            }
            C(row, col) = inner_product;
        }
    }
    newB.free();
}

int CACHE_CUTOFF = 200;

void rec_mult_with_naive_base2(const Matrix& A, const Matrix& B, Matrix& C) {
    assert(A.columns() == B.rows());
    assert(C.rows() == A.rows() && C.columns() == B.columns());
    size_t m = A.rows();
    size_t n = A.columns();
    size_t p = B.columns();

    for(size_t row = 0; row <= C.rows() / CACHE_CUTOFF; row++) {
        for(size_t col = 0; col <= C.columns() / CACHE_CUTOFF; col++) {
            for(size_t k = 0; k <= A.columns() / CACHE_CUTOFF; k++) {
                int rowSize = CACHE_CUTOFF;
                if (row == C.rows() / CACHE_CUTOFF) {
                    rowSize = C.rows() - (row * CACHE_CUTOFF);
                }
                int colSize = CACHE_CUTOFF;
                if (col == C.columns() / CACHE_CUTOFF) {
                    colSize = C.columns() - (col * CACHE_CUTOFF);
                }
                int kSize = CACHE_CUTOFF;
                if (k == A.columns() / CACHE_CUTOFF) {
                    kSize = A.columns() - (k * CACHE_CUTOFF);
                }
                const Matrix subA(A.data(), A.subview(row * CACHE_CUTOFF, k * CACHE_CUTOFF, rowSize, kSize));
                const Matrix subB(B.data(), B.subview(k * CACHE_CUTOFF, col * CACHE_CUTOFF, kSize, colSize));
                Matrix subC(C.data(), C.subview(row * CACHE_CUTOFF, col * CACHE_CUTOFF, rowSize, colSize));
                mult(subA, subB, subC);
            }
        }
    }
}

void rec_mult_with_transposing_base2(const Matrix& A, const Matrix& B, Matrix& C) {
    assert(A.columns() == B.rows());
    assert(C.rows() == A.rows() && C.columns() == B.columns());
    size_t m = A.rows();
    size_t n = A.columns();
    size_t p = B.columns();

    for(size_t row = 0; row <= C.rows() / CACHE_CUTOFF; row++) {
        for(size_t col = 0; col <= C.columns() / CACHE_CUTOFF; col++) {
            for(size_t k = 0; k <= A.columns() / CACHE_CUTOFF; k++) {
                int rowSize = CACHE_CUTOFF;
                if (row == C.rows() / CACHE_CUTOFF) {
                    rowSize = C.rows() - (row * CACHE_CUTOFF);
                }
                int colSize = CACHE_CUTOFF;
                if (col == C.columns() / CACHE_CUTOFF) {
                    colSize = C.columns() - (col * CACHE_CUTOFF);
                }
                int kSize = CACHE_CUTOFF;
                if (k == A.columns() / CACHE_CUTOFF) {
                    kSize = A.columns() - (k * CACHE_CUTOFF);
                }
                const Matrix subA(A.data(), A.subview(row * CACHE_CUTOFF, k * CACHE_CUTOFF, rowSize, kSize));
                const Matrix subB(B.data(), B.subview(k * CACHE_CUTOFF, col * CACHE_CUTOFF, kSize, colSize));
                Matrix subC(C.data(), C.subview(row * CACHE_CUTOFF, col * CACHE_CUTOFF, rowSize, colSize));
                transposeMult(subA, subB, subC);
            }
        }
    }
}


void rec_mult_with_naive_base(const Matrix& A, const Matrix& B, Matrix& C) {
    assert(A.columns() == B.rows());
    assert(C.rows() == A.rows() && C.columns() == B.columns());

    size_t m = A.rows();
    size_t n = A.columns();
    size_t p = B.columns();

    if (m == 1 && n == 1 && p == 1) {
        // Base case, simple scalar multiplication.
        C(0, 0) += A(0, 0) * B(0, 0);
    } else if (A.rows() + A.columns() < CACHE_CUTOFF) {
        mult(A, B, C);
    } else if(m >= std::max(n, p)) {
        // Create two new matrices A1 and A2 with each half of the rows of A.
        size_t split = m / 2;

        const Matrix A1(A.data(), A.subview(0, 0, split, A.columns()));
        const Matrix A2(A.data(), A.subview(split, 0, m - split, A.columns()));

        Matrix C1(C.data(), C.subview(0, 0, split, C.columns()));
        Matrix C2(C.data(), C.subview(split, 0, m - split, C.columns()));

        rec_mult_with_naive_base(A1, B, C1);
        rec_mult_with_naive_base(A2, B, C2);
    } else if(n >= std::max(m, p)) {
        // Create two new matrices A1 and A2 with each half of the columns of A.
        size_t split = n / 2;

        const Matrix A1(A.data(), A.subview(0, 0, A.rows(), split));
        const Matrix A2(A.data(), A.subview(0, split, A.rows(), n - split));

        // Create two new matrices B1 and B2 with each half of the rows of B.
        const Matrix B1(B.data(), B.subview(0, 0, split, B.columns()));
        const Matrix B2(B.data(), B.subview(split, 0, n - split, B.columns()));

        rec_mult_with_naive_base(A1, B1, C);
        rec_mult_with_naive_base(A2, B2, C);
    } else if(p >= std::max(m, n)) {
        size_t split = p / 2;

        const Matrix B1(B.data(), B.subview(0, 0, B.rows(), split));
        const Matrix B2(B.data(), B.subview(0, split, B.rows(), p - split));

        Matrix C1(C.data(), C.subview(0, 0, C.rows(), split));
        Matrix C2(C.data(), C.subview(0, split, C.rows(), p - split));

        rec_mult_with_naive_base(A, B1, C1);
        rec_mult_with_naive_base(A, B2, C2);
    } else {
        assert(0);
    }
}

void rec_mult_with_transposing_base(const Matrix& A, const Matrix& B, Matrix& C) {
    assert(A.columns() == B.rows());
    assert(C.rows() == A.rows() && C.columns() == B.columns());

    size_t m = A.rows();
    size_t n = A.columns();
    size_t p = B.columns();

    if (m == 1 && n == 1 && p == 1) {
        // Base case, simple scalar multiplication.
        C(0, 0) += A(0, 0) * B(0, 0);
    } else if (A.rows() + A.columns() < CACHE_CUTOFF) {
        transposeMult(A, B, C);
    } else if(m >= std::max(n, p)) {
        // Create two new matrices A1 and A2 with each half of the rows of A.
        size_t split = m / 2;

        const Matrix A1(A.data(), A.subview(0, 0, split, A.columns()));
        const Matrix A2(A.data(), A.subview(split, 0, m - split, A.columns()));

        Matrix C1(C.data(), C.subview(0, 0, split, C.columns()));
        Matrix C2(C.data(), C.subview(split, 0, m - split, C.columns()));

        rec_mult_with_transposing_base(A1, B, C1);
        rec_mult_with_transposing_base(A2, B, C2);
    } else if(n >= std::max(m, p)) {
        // Create two new matrices A1 and A2 with each half of the columns of A.
        size_t split = n / 2;

        const Matrix A1(A.data(), A.subview(0, 0, A.rows(), split));
        const Matrix A2(A.data(), A.subview(0, split, A.rows(), n - split));

        // Create two new matrices B1 and B2 with each half of the rows of B.
        const Matrix B1(B.data(), B.subview(0, 0, split, B.columns()));
        const Matrix B2(B.data(), B.subview(split, 0, n - split, B.columns()));

        rec_mult_with_transposing_base(A1, B1, C);
        rec_mult_with_transposing_base(A2, B2, C);
    } else if(p >= std::max(m, n)) {
        size_t split = p / 2;

        const Matrix B1(B.data(), B.subview(0, 0, B.rows(), split));
        const Matrix B2(B.data(), B.subview(0, split, B.rows(), p - split));

        Matrix C1(C.data(), C.subview(0, 0, C.rows(), split));
        Matrix C2(C.data(), C.subview(0, split, C.rows(), p - split));

        rec_mult_with_transposing_base(A, B1, C1);
        rec_mult_with_transposing_base(A, B2, C2);
    } else {
        assert(0);
    }
}


double test(Test test) {
    // Garbage
    int subResult = 0;

    srand(9853287); // Deterministic random

    test.initialize();

    srand(56189174); // Very deterministic

    // Running once, for startup.
    subResult = (int) test.query();

    clock_t startTime = clock();

#ifdef PAPI
    // Start papi counters for the desired events...
    int PAPI_events[NUMBER_OF_EVENTS] = EVENTS;
    if(PAPI_start_counters(PAPI_events, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to start counters." << std::endl;
        return 0;
    }
#endif

    for (int i = 0; i < REPS; i++) {
        uint64_t result = test.query();
        subResult += result;
    }

    clock_t endTime = clock();

//    cout << test.name << ": " << subResult << endl;

    sum += subResult;

    A->free();
    free(A);
    B->free();
    free(B);
    C->free();
    free(C);

#ifdef PAPI
    // Stop the papi counters, reading the counter values.
    long long values[NUMBER_OF_EVENTS];
    if(PAPI_stop_counters(values, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to stop counters." << std::endl;
        return 0;
    }
#endif

    // Print the result:
    /*for(int i = 0; i < NUMBER_OF_EVENTS; i++) {
        std::cout << "Event " << PAPIEventString(PAPI_events[i]) << " had counter value: " << values[i] << "." << std::endl;
    }*/

#ifdef PAPI
    return values[0];
#else
    double time = (endTime - startTime) / (1.0 * CLOCKS_PER_SEC);
    return time;
#endif
}

void initializeMatricies() {
    MatrixData A_data(N, N, new element_t[N * N]);
    A = new Matrix(A_data, A_data.view());
    setRandomValues(*(A));   
    
    MatrixData B_data(N, N, new element_t[N * N]);
    B = new Matrix(B_data, B_data.view());
    setRandomValues(*(B));   
    
    MatrixData C_data(N, N, new element_t[N * N]);
    C = new Matrix(C_data, C_data.view());
    setRandomValues(*(C));   
}

int naiveMatrixMult() {
    mult(*(A), *(B), *(C));
    return 4; // So the compiler doesn't optimize everthing away. 
}

int recursiveMatrixMult() {
    rec_mult(*(A), *(B), *(C));
    return 5;
}

int cacheAwareMult() {
    rec_mult_with_naive_base(*(A), *(B), *(C));
    return 6;
}

int tiles() {
    rec_mult_with_naive_base2(*(A), *(B), *(C));
    return 9;
}

int tiles_trans() {
    rec_mult_with_transposing_base2(*(A), *(B), *(C));
    return 9;
}

int cacheAwareTransposingMult() {
    rec_mult_with_transposing_base(*(A), *(B), *(C));
    return 8;
}

int trasposingMult() {
    transposeMult(*(A), *(B), *(C));
    return 7;
}


int main() {
#ifdef PAPI
    // Fetch the number of counters available. This also initializes PAPI
    int num_counters = PAPI_num_counters(); // papi

    std::cout << "This system has: " << num_counters << " available counters." << std::endl;

    if(NUMBER_OF_EVENTS > num_counters) {
        std::cerr << "Not enough PAPI counters available! Need: " << NUMBER_OF_EVENTS << "." << std::endl;
        return 0;
    }
#endif

    std::vector<Test> tests;

//    tests.push_back(Test("Garbage",  cacheAwareTransposingMult, initializeMatricies));
    //tests.push_back(Test("Recursive",  recursiveMatrixMult, initializeMatricies));

//    tests.push_back(Test("Naive", naiveMatrixMult, initializeMatricies));
    tests.push_back(Test("Cache aware",  cacheAwareMult, initializeMatricies));
//    tests.push_back(Test("Transposing",  trasposingMult, initializeMatricies));
    tests.push_back(Test("Cache aware transposing",  cacheAwareTransposingMult, initializeMatricies));
    tests.push_back(Test("tiles", tiles, initializeMatricies));
    tests.push_back(Test("tiles transposing", tiles_trans, initializeMatricies));

    std::cout.precision(15);
    std::cout.imbue(std::locale(std::cout.getloc(), new DecimalSeparator<char>(',')));

    std::cout << "N";
    for (uint i = 0; i < tests.size(); i++) {
        std::cout << ";" << tests.at(i).name;
    }
    std::cout << std::endl;

    N = 100;

    while(N < 100000000) {
        std::cout << N;
//        std::cout << CACHE_CUTOFF;
        for (uint i = 0; i < tests.size(); i++) {
            std::cout << ";";
            std::cout.flush();
            double result = test(tests.at(i));
            std::cout << result;
        }
//        CACHE_CUTOFF += 10;
        N += 100;
        std::cout << std::endl;
    }
    std::cout << "Garbage: " << sum << std::endl;
    return 0;
}