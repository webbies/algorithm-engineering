#include <iostream>
#include <vector>
#include <assert.h>
#include <cmath>
#include <stdint.h>
#include <stdlib.h>

#define REPS 1000000
int N;

class ConstantTimeSelect;
class BitVector;
size_t bruteforceSelect(BitVector const &B, size_t q);

template<typename T>
void printBinaryString(T S, int number_of_bits) {
    for(size_t i = 0; i < number_of_bits; i++) {
        if(S & (1 << (number_of_bits - 1 - i))) {
            std::cout << "1";
        } else {
            std::cout << "0";
        }
    }
    std::cout << std::endl;
}

class BitVector {

    size_t _size;
    std::vector<uint8_t> _table;

public:
    explicit BitVector() : _size(0) {}

    explicit BitVector(size_t size) : _size(size) {
        resize(size);
    }

    void resize(size_t size) {
        _table.resize((size - 1) / 8 + 1);
        for(auto& c : _table) {
            c = 0;
        }
    }

    bool get(size_t index) const {
        size_t word_index = index / 8;
        size_t rem = 7 - (index - word_index * 8);

        uint8_t word = _table[word_index];

        bool result = (word & (1 << rem)) != 0;
        return result;
    }

    void set(size_t index, bool value) {
        size_t word_index = index / 8;
        size_t rem = 7 - (index - word_index * 8);

        if(value) {
            _table[word_index] |= (1 << rem);
        } else {
            _table[word_index] &= ~(1 << rem);
        }
    }

    uint32_t getSubstring(int index, uint64_t length) const {
        size_t word_index = index / 8;
        uint64_t rem = 7ULL - (index - word_index * 8ULL);

        uint64_t val = 0;
        for(int i = 0; i < 8; i++) {
            if(_table.size() > word_index + i) {
                val |= uint64_t(_table[word_index + i]) << (64ULL - (i + 1ULL) * 8ULL);
            }
        }

        uint64_t mask = ((1ULL << length) - 1ULL) << (64ULL - (7 - rem) - length);

        uint64_t res = (val & mask) >> (64ULL - (7 - rem) - length);

        return uint32_t(res);
    }

    size_t size() const { return _size; }

};

// Computes the base 2 logarithm floored.
uint8_t ilog2(uint64_t value) {
    uint8_t depth = -1;
    while (value != 0) {
        value >>= 1, depth++;
    }
    return depth;
}

int bruteforceRank(const BitVector& S, size_t index) {
    int result = 0;

    for(size_t i = 0; i < index; i++) {
        if(S.get(i)) {
            result++;
        }
    }

    return result;
}

int rank(const BitVector& B, size_t s, size_t b, const std::vector<int>& Rs, const std::vector<int>& Rb, std::vector<std::vector<int> > Rps, size_t i) {
    int start_index = (i / b) * b;

    /*// number consisting of b bits:
    uint32_t substring = 0;


    for(size_t i2 = 0; i2 < b; i2++) {
        if(B.get(start_index + i2)) {
            substring |= (1U << (b - i2 - 1U));
        }
    }*/

    uint32_t substring = B.getSubstring(start_index, b);

    return Rs[i / s] + Rb[i / b] + Rps[substring][i % b];
}

size_t binarySearchSelect(const BitVector& B, size_t q) {

    size_t lower = 0, higher = B.size();

    size_t pivot;
    while(lower != higher) {

        size_t new_pivot = (lower + higher) / 2;

        if(new_pivot == pivot) {
            break;
        }

        pivot = new_pivot;

        int rank1 = bruteforceRank(B, pivot);

        if(pivot == 0) {
            return 0;
        }

        int rank2 = bruteforceRank(B, pivot - 1);

        if(rank1 == q && rank2 == q - 1) {
            return pivot;
        }

        if(rank1 == q && rank2 == q) {
            higher = pivot;
        } else {
            if(rank1 < q) {
                lower = pivot;
            } else {
                higher = pivot;
            }
        }
    }


    return -1;
}

#define DEBUG_COUNTERS 1

class ConstantTimeSelect {
    size_t _n;

    BitVector m_B;

    size_t _number_of_ones;

    size_t _b;  // lg_2(n) / c
    size_t _lnlln;  // lg_2(n) * lg_2(lg_2(n))

    std::vector<size_t> _population;

    std::vector<std::vector<size_t> > _selection;

    std::vector<size_t> _L1;
    std::vector<std::vector<size_t> > _L2;
    std::vector<std::vector<std::vector<size_t> > > _L3;

#if DEBUG_COUNTERS
    size_t L2_lookup_counter, L3_lookup_counter, weird_lookup_count;
#endif

public:
    explicit ConstantTimeSelect(size_t n) : _n(n),

            m_B(n),

            _b(ilog2(n) / 2),
            _lnlln(ilog2(n) * ilog2(ilog2(n))),

            _population(1 << _b),

            _selection(1 << _b),

            _L1(_n / _lnlln + 1),
            _L2(_n / _lnlln + 1),
            _L3(_n / _lnlln + 1)


    {

    }

    size_t population(size_t val, size_t number_of_bits) {
        size_t result = 0;

        for(size_t i = 0; i < number_of_bits; i++) {
            if(val & 1) {
                result++;
            }
            val >>= 1;
        }

        return result;
    }

    void init(double ratio) {
        int one_counter = 0;
        int seed = 3456879;

        for(size_t i = 0; i < _n; i++) {
            if (i < _n * ratio) {
                m_B.set(i, 1);
            } else {
                m_B.set(i, 0);
            }
        }

        srand(124785151);
        for (size_t i = 2; i < _n; i++) {
            size_t j = (rand() % (i - 1)) + 1; // Swap with one in index [1, i-1]
            bool tmp = m_B.get(i);
            m_B.set(i, m_B.get(j));
            m_B.set(j, tmp);
        }

   
        _number_of_ones = 0;
        for(size_t i = 0; i < _n; i++) {
            if(m_B.get(i)) {
                _number_of_ones++;
            }
        }

//        std::cout << "Total number of 1s:" << _number_of_ones << std::endl;

        for(size_t i = 0; i < 1 << _b; i++) {
            size_t pop = population(i, _b);

            _population[i] = pop;
        }

        for(size_t i = 0; i < 1 << _b; i++) {
            _selection[i].resize(_b);

            size_t selection_index = 0;

            size_t mask = 1 << (_b - 1);

            for(size_t j = 0; j < _b; j++) {
                if(i & mask) {
                    _selection[i][selection_index] = j + 1;
                    selection_index++;
                }
                mask >>= 1;
            }

            for(size_t rem = selection_index; rem < _b; rem++) {
                _selection[i][selection_index] = -1;
            }

        }

        for(size_t i = 0; i < _L1.size(); i++) {
            _L1[i] = bruteforceSelect(m_B, i * _lnlln);

            // Speed up "out of bounds simplification"
            if(_L1[i] == m_B.size() + 1) {
                i++;
                for(; i < _L1.size(); i++) {
                    _L1[i] = m_B.size() + 1;
                }
            }
        }

        for(size_t i = 0; i < _L1.size() - 1; i++) {

            size_t R = _L1[i + 1] - _L1[i];

            if(R > _lnlln * _lnlln) {
                // Explicit calculation
                _L2[i].resize(_lnlln + 1);

                size_t one_index = 0;
                _L2[i][one_index] = _L1[i];
                one_index++;

                for(size_t j = _L1[i]; j < _L1[i + 1]; j++) {
                    if(m_B.get(j) == 1) {
                        _L2[i][one_index] = j + 1;
                        one_index++;
                    }
                }

                assert(one_index <= _L2[i].size());

            } else {
                // Implicit calculation
                _L2[i].resize(R / (ilog2(R) * ilog2(ilog2(_n))) + 2);    // approximately
                _L3[i].resize(_L2[i].size());

                for(size_t j = 0; j < _L2[i].size(); j++) {
                    _L2[i][j] = bruteforceSelect(m_B, i * _lnlln + j * ilog2(R) * ilog2(ilog2(_n)));

                    // Speed up "out of bounds simplification"
                    if(_L2[i][j] == m_B.size() + 1) {
                        j++;
                        for(; j < _L2[i].size(); j++) {
                            _L2[i][j] = m_B.size() + 1;
                        }
                    }
                }



                for(size_t k = 0; k < _L2[i].size() - 1; k++) {

                    size_t R_prime = _L2[i][k + 1] - _L2[i][k];

                    if (R_prime > ilog2(R_prime) * ilog2(R) * ilog2(ilog2(_n)) * ilog2(ilog2(_n))) {
                        // Explicit calculation
                        size_t mini_one_index = 0;

                        _L3[i][k].push_back(_L2[i][k]);

                        for(size_t l = _L2[i][k]; l < _L2[i][k + 1]; l++) {
                            if(m_B.get(l) == 1) {
                                _L3[i][k].push_back(l + 1);
                            }
                        }

                    } else {
                        // Implicit calculation


                    }
                }
            }

        }

    }

    int select(size_t query) {
        if(query > _number_of_ones) {
            return m_B.size() + 1;
        }

        size_t L1_index = query / _lnlln;
        size_t L1_rem = query - L1_index * _lnlln;

        size_t R = _L1[L1_index + 1] - _L1[L1_index];
        if(R > _lnlln * _lnlln) {   // answer is in table.
#if DEBUG_COUNTERS
            L2_lookup_counter++;
#endif
            return _L2[L1_index][L1_rem];
        } else {
            size_t L2_index = L1_rem / (ilog2(R) * ilog2(ilog2(_n)));
            size_t L2_rem = L1_rem - L2_index * ilog2(R) * ilog2(ilog2(_n));

            size_t L2_select_a = _L2[L1_index][L2_index];
            size_t L2_select_b = _L2[L1_index][L2_index + 1];

            size_t R_prime = L2_select_b - L2_select_a;

            if (R_prime > ilog2(R_prime) * ilog2(R) * ilog2(ilog2(_n)) * ilog2(ilog2(_n))) {
#if DEBUG_COUNTERS
                L3_lookup_counter++;
#endif
                return _L3[L1_index][L2_index][L2_rem];
            } else {
#if DEBUG_COUNTERS
                weird_lookup_count++;
#endif
                size_t start_index = L2_select_a;

                size_t number_of_ones = L1_index * _lnlln + L2_index * ilog2(R) * ilog2(ilog2(_n));

                while(start_index < L2_select_b) {
                    size_t shift = 0;
                    size_t end_index = start_index + _b;
                    if(end_index > L2_select_b) {
                        shift = end_index - L2_select_b;
                        end_index = L2_select_b;
                    }

                    uint32_t substr = m_B.getSubstring(start_index, end_index - start_index) << shift;

                    size_t pop = _population[substr];

                    if(number_of_ones + pop >= query) {
                        if(query - number_of_ones != 0) {
                            return start_index + _selection[substr][query - number_of_ones - 1];
                        } else {
                            return start_index;
                        }
                    } else {
                        number_of_ones += pop;
                    }

                    start_index += _b;
                }
                std::cout << "THIS IS AWFUL! OUT OF BOUNDS BUT WE ALREADY CHECKED THIS IN BEGINNING" << std::endl;
                assert(0);
            }
        }
    }

    void printDebug() {
        std::cout << "L2: " << L2_lookup_counter << " L3: " << L3_lookup_counter << " " << weird_lookup_count << std::endl;
    }

    size_t n() { return _n; }

    const BitVector& B() { return m_B; }

};

class ConstantTimeRank {
    size_t _n;

    BitVector m_B;

    size_t _b, _s;

    std::vector<int> _Rs;
    std::vector<int> _Rb;

    std::vector<std::vector<int> > _Rps;

public:
    explicit ConstantTimeRank(size_t n) : _n(n),

                                          m_B(n),

                                          _b(ilog2(n) / 2),
                                          _s(_b * ilog2(n)),

                                          _Rs(n / _s + 1),
                                          _Rb(n / _b + 1),
                                          _Rps(1 << _b) {

    }

    void init(double ratio) {
        for(size_t i = 0; i < _n; i++) {
            if (i < _n * ratio) {
                m_B.set(i, 1);
            } else {
                m_B.set(i, 0);
            }
        }

        srand(124785151);
        for (size_t i = 2; i < _n; i++) {
            size_t j = (rand() % (i - 1)) + 1; // Swap with one in index [1, i-1]
            bool tmp = m_B.get(i);
            m_B.set(i, m_B.get(j));
            m_B.set(j, tmp);
        }

        // Number for each super block:
        // Rs[j] = rank(B, j * s)
        for(size_t j = 0; j < _n / _s + 1; j++) {
            _Rs[j] = bruteforceRank(m_B, j * _s);
        }

        // Number for each basic block:
        // Rb[k] = rank(B, k * b) - rank(B, j * s)
        // (j is the super block of the basic block)
        for(size_t k = 0; k < _n / _b + 1; k++) {
            // Super block index:
            size_t j = k / ilog2(_n);
            _Rb[k] = bruteforceRank(m_B, k * _b) - _Rs[j];
        }

        for(size_t S = 0; S < 1 << _b; S++) {
            // Convert S to a BitVector:
            BitVector bv_S(_b);
            for(size_t i = 0; i < _b; i++) {
                bv_S.set(i, (S & (1 << (_b - i - 1))) != 0);
            }

            std::vector<int>& Rp = _Rps[S];
            Rp.resize(_b);

            // Rp[S, i] = rank(S, i)
            Rp[0] = 0;
            for(size_t i = 1; i < _b; i++) {
                if(bv_S.get(i - 1)) {
                    Rp[i] = Rp[i - 1] + 1;
                } else {
                    Rp[i] = Rp[i - 1];
                }

                assert(Rp[i] == bruteforceRank(bv_S, i));
            }
        }
    }

    int rank(int i) const {
        int start_index = (i / _b) * _b;

        uint32_t substring = m_B.getSubstring(start_index, _b);

        return _Rs[i / _s] + _Rb[i / _b] + _Rps[substring][i % _b];
    }

    size_t n() { return _n; }

    const BitVector& B() { return m_B; }
};

ConstantTimeSelect ctr(5);

void initializeSelect() {
    ctr = ConstantTimeSelect(N);
    ctr.init(0.5);
}

int smartSelect(size_t q) {
    return ctr.select(q);
}

size_t bruteforceSelect(BitVector const &B, size_t q) {
    size_t count = 0;
    for(size_t i = 0; i < B.size(); i++) {
        if(count == q) {
            return i;
        }
        if(B.get(i)) {
            count++;
        }
    }
    if(q == count) {
        return B.size();
    }
    return B.size() + 1;  // Out of bounds.
}

typedef int(*QueryFunction)(size_t q);
typedef void(*InitializeFunction)(void);
struct Test {
    Test(std::string name, QueryFunction query, InitializeFunction initialize) : name(name), query(query), initialize(initialize) { }
    std::string name;
    QueryFunction query;
    InitializeFunction initialize;
};

// Getting comma as decimal seperator, thanks to: http://stackoverflow.com/questions/1422151/how-to-print-a-double-with-a-comma
template<typename CharT> class DecimalSeparator : public std::numpunct<CharT> {
public:
    DecimalSeparator(CharT Separator) : m_Separator(Separator) {}
    CharT do_decimal_point()const {return m_Separator;}
    CharT m_Separator;
};

int sum = 0;

double test(Test test) {
    // Garbage
    int subResult = 0;

    srand(9853287); // Deterministic random

    test.initialize();

    srand(56189174); // Very deterministic

    // Running once, for startup.
     subResult = (int) test.query(rand() % (int)(N * 0.5));

    clock_t startTime = clock();

#ifdef PAPI
    // Start papi counters for the desired events...
    int PAPI_events[NUMBER_OF_EVENTS] = EVENTS;
    if(PAPI_start_counters(PAPI_events, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to start counters." << std::endl;
        return 0;
    }
#endif

    for (int i = 0; i < REPS; i++) {
        int result = test.query(rand() % (int)(N * 0.5));
        subResult += result;
    }

    clock_t endTime = clock();

//    cout << test.name << ": " << subResult << endl;

    sum += subResult;


#ifdef PAPI
    // Stop the papi counters, reading the counter values.
    long long values[NUMBER_OF_EVENTS];
    if(PAPI_stop_counters(values, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to stop counters." << std::endl;
        return 0;
    }
#endif

    // Print the result:
    /*for(int i = 0; i < NUMBER_OF_EVENTS; i++) {
        std::cout << "Event " << PAPIEventString(PAPI_events[i]) << " had counter value: " << values[i] << "." << std::endl;
    }*/

#ifdef PAPI
    return values[0];
#else
    double time = (endTime - startTime) / (1.0 * CLOCKS_PER_SEC);
    return time;
#endif
}

int main() {
#ifdef PAPI
    // Fetch the number of counters available. This also initializes PAPI
    int num_counters = PAPI_num_counters(); // papi

    std::cout << "This system has: " << num_counters << " available counters." << std::endl;

    if(NUMBER_OF_EVENTS > num_counters) {
        std::cerr << "Not enough PAPI counters available! Need: " << NUMBER_OF_EVENTS << "." << std::endl;
        return 0;
    }
#endif

    std::vector<Test> tests;

    tests.push_back(Test("SmartSelect", smartSelect, initializeSelect));

    std::cout.precision(15);
    std::cout.imbue(std::locale(std::cout.getloc(), new DecimalSeparator<char>(',')));

    std::cout << "N";
    for (uint i = 0; i < tests.size(); i++) {
        std::cout << ";" << tests.at(i).name;
    }
    std::cout << std::endl;

    N = 10000;

    while(N < 1000000) {
        std::cout << N;
        for (uint i = 0; i < tests.size(); i++) {
            std::cout << ";";
            std::cout.flush();
            double result = test(tests.at(i));
            std::cout << result;
        }
//        N += 100;
        N = N * 1.1;
        std::cout << std::endl;
    }
    std::cout << "Garbage: " << sum << std::endl;
    return 0;
}