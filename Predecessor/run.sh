#!/bin/bash    
g++ -std=c++11 -c $1.cpp -O3
g++ $1.o -lpapi -o $1
echo "Compiled $1, now running. "
./main
