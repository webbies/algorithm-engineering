#include <papi.h>

#include <iostream>
#include <array>
#include <stdexcept>

// Prefixed by L1, L2 and L3:
	// DCM = Data Cache Misses
	// ICM = Instruction Cache Misses
	// TCM = Total Cache Misses

// Prefixed by L1 and L2:
	// LDM = load misses
	// STM = store misses

// Prefixed by TLB
	// DM = Data Translation
	// IM = Instruction Translation

#define NUMBER_OF_EVENTS 2
#define EVENTS { PAPI_L1_DCM, PAPI_L2_DCM }

//#define TABLE_SIZE 1048576	// 2^20
//#define TABLE_SIZE 8388608	// 2^23
#define TABLE_SIZE 16777216	// 2^24
//#define TABLE_SIZE 33554432	// 2^25

size_t _table[TABLE_SIZE];

std::string PAPIEventString(int event) {
    switch(event) {
        case PAPI_L1_DCM:
            return "PAPI_L1_DCM";
        case PAPI_L2_DCM:
            return "PAPI_L2_DCM";
        case PAPI_L3_DCM:
            return "PAPI_L3_DCM";
    }
    throw std::runtime_error("Unimplemented PAPI event string conversion");
}

int main() {
    // Fetch the number of counters available.
    int num_counters = PAPI_num_counters();

    std::cout << "This system has: " << num_counters << " available counters." << std::endl;

    if(NUMBER_OF_EVENTS > num_counters) {
        std::cerr << "Not enough PAPI counters available! Need: " << NUMBER_OF_EVENTS << "." << std::endl;
        return 0;
    }

    // Start counters for the desired events...
    int PAPI_events[NUMBER_OF_EVENTS] = EVENTS;
    if(PAPI_start_counters(PAPI_events, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to start counters." << std::endl;
        return 0;
    }

    // Do the code we want to measure:
    for(int i = 0; i < TABLE_SIZE; i++) {
        _table[i] = ((i % 7) << (i % 10)) ^ (i % 4);
    }

    // Stop the counters, reading the counter values.
    long long values[NUMBER_OF_EVENTS];
    if(PAPI_stop_counters(values, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to stop counters." << std::endl;
        return 0;
    }

    // Print the result:
    for(int i = 0; i < NUMBER_OF_EVENTS; i++) {
        std::cout << "Event " << PAPIEventString(PAPI_events[i]) << " had counter value: " << values[i] << "." << std::endl;
    }
}