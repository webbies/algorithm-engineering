// #define PAPI
// http://icl.cs.utk.edu/projects/papi/wiki/PAPI3:PAPI_presets.3
#include <algorithm>
#include <iostream>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <ctime>
#include <string.h>
#include <stdint.h>
#include <stdint-gcc.h>
#include <iomanip>
#include <vector>
//#include <sys/unistd.h>
#ifdef PAPI
#include <papi.h>
#endif
#include <array>
#include <stdexcept>
#include <cassert>
#include <sys/unistd.h>

// Prefixed by L1, L2 and L3:
// DCM = Data Cache Misses
// ICM = Instruction Cache Misses
// TCM = Total Cache Misses

// Prefixed by L1 and L2:
// LDM = load misses
// STM = store misses

// Prefixed by TLB
// DM = Data Translation
// IM = Instruction Translation


#define NUMBER_OF_EVENTS 1
#define EVENTS {PAPI_L2_DCM }

using namespace std;

uint64_t queryLinearPred(uint64_t query);

void initializeSortedArray();

uint64_t queryBinaryPred(uint64_t query) ;

uint64_t queryMultiBinaryPred(uint64_t query) ;

uint64_t queryMultiBinaryPredEarlyExit(uint64_t query) ;

void initializeDfs();

uint64_t queryDfs(uint64_t query);

void initializeDfs2();

uint64_t queryDfs2(uint64_t query);

void initializeBfs();

uint64_t queryBfs(uint64_t query);

void initializeDfsPointers();

uint64_t queryPointers(uint64_t query);

void initializeRandomPointers();

void initializeSkewedDfsPointers();
void initializeSkewedBfsPointers();

typedef uint64_t(*QueryFunction)(uint64_t);
typedef void(*InitializeFunction)(void);
struct Test {
    Test(string name, QueryFunction query, InitializeFunction initialize) : name(name), query(query), initialize(initialize) { }
    string name;
    QueryFunction query;
    InitializeFunction initialize;
};

double test(Test test);


uint64_t N = 14; // 2^27 -1 == 134217727
const uint64_t REPS = 1000000;

double skewedNess = 0.9;

void * pointer;

struct Node {
    uint64_t value;
    size_t left;
    size_t right;
    Node(uint64_t value, size_t left, size_t right) : value(value), left(left), right(right) {

    }
};

int sum = 0;

// Getting comma as decimal seperator, thanks to: http://stackoverflow.com/questions/1422151/how-to-print-a-double-with-a-comma
template<typename CharT> class DecimalSeparator : public std::numpunct<CharT> {
public:
    DecimalSeparator(CharT Separator) : m_Separator(Separator) {}
    CharT do_decimal_point()const {return m_Separator;}
    CharT m_Separator;
};

int main() {
#ifdef PAPI
    // Fetch the number of counters available. This also initializes PAPI
    int num_counters = PAPI_num_counters(); // papi

    std::cout << "This system has: " << num_counters << " available counters." << std::endl;

    if(NUMBER_OF_EVENTS > num_counters) {
        std::cerr << "Not enough PAPI counters available! Need: " << NUMBER_OF_EVENTS << "." << std::endl;
        return 0;
    }
    #endif

    vector<Test> tests;

//tests.push_back(Test("pred linear", queryLinearPred, initializeSortedArray));
    tests.push_back(Test("garbage", queryBinaryPred, initializeSortedArray));

    tests.push_back(Test("binary", queryBinaryPred, initializeSortedArray));
    tests.push_back(Test("multi", queryMultiBinaryPred, initializeSortedArray));
    tests.push_back(Test("early", queryMultiBinaryPredEarlyExit, initializeSortedArray));
    tests.push_back(Test("pred dfs", queryDfs, initializeDfs));
    tests.push_back(Test("dfs pointers", queryPointers, initializeDfsPointers));
    tests.push_back(Test("random pointers", queryPointers, initializeRandomPointers));
    tests.push_back(Test("pred bfs", queryBfs, initializeBfs));
    tests.push_back(Test("dfs2", queryDfs2, initializeDfs2));
//    tests.push_back(Test("skewed dfs pointers", queryPointers, initializeSkewedDfsPointers));
//    tests.push_back(Test("skewed bfs pointers", queryPointers, initializeSkewedBfsPointers));

    cout.precision(15);
    std::cout.imbue(std::locale(std::cout.getloc(), new DecimalSeparator<char>(',')));

    cout << "N";
    for (int i = 0; i < tests.size(); i++) {
        cout << ";" << tests.at(i).name;
    }
    cout << endl;

    skewedNess = 0.50;

     N = 63;
//    N = 67108863;

    while(N < 67108865 - 1) {
        cout << N;
//        cout << skewedNess;
        for (int i = 0; i < tests.size(); i++) {
            cout << ";";
            cout.flush();
            double result = test(tests.at(i));
            cout << result;
        }
        N = ((N + 1) * 2) - 1;
//        skewedNess += 0.01;

        cout << endl;
    }
    cout << "Garbage: " << sum << endl;
    return 0;
}

double test(Test test) {
    // Garbage, that gets printed.
    int subResult = 0;

    srand(9853287); // Deterministic random

    test.initialize();

    srand(56189174); // Very deterministic

    // Running once, for startup.
    uint64_t query = rand() % N;
    subResult = (int) test.query(query);

    clock_t startTime = clock();

#ifdef PAPI
    // Start papi counters for the desired events...
    int PAPI_events[NUMBER_OF_EVENTS] = EVENTS;
    if(PAPI_start_counters(PAPI_events, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to start counters." << std::endl;
        return 0;
    }
#endif

    for (int i = 0; i < REPS; i++) {
        uint64_t query = rand() % N;
        uint64_t result = test.query(query);
        subResult += result;
    }

    clock_t endTime = clock();

//    cout << endl << test.name << ": " << subResult << endl;

    sum += subResult;

    free(pointer);

#ifdef PAPI
    // Stop the papi counters, reading the counter values.
    long long values[NUMBER_OF_EVENTS];
    if(PAPI_stop_counters(values, NUMBER_OF_EVENTS) != PAPI_OK) {
        std::cerr << "Failed to stop counters." << std::endl;
        return 0;
    }
#endif

    // Print the result:
    /*for(int i = 0; i < NUMBER_OF_EVENTS; i++) {
        std::cout << "Event " << PAPIEventString(PAPI_events[i]) << " had counter value: " << values[i] << "." << std::endl;
    }*/

    //return values[0];

    double time = (endTime - startTime) / (1.0 * CLOCKS_PER_SEC);
    return time;
}

void recursivelyFillLayer(int *layers, Node *nodes, size_t index, int layer) {
    layers[index] = layer;
    Node node = nodes[index];
    if (node.left != 0) {
        recursivelyFillLayer(layers, nodes, node.left, layer + 1);
    }
    if (node.right != 0) {
        recursivelyFillLayer(layers, nodes, node.right, layer + 1);
    }
}


void initializeSkewedBfsPointers() {
    initializeSkewedDfsPointers();
    int *layers = (int *) malloc(sizeof(int) * N);
    Node *Nodes = (Node *) pointer;
    recursivelyFillLayer(layers, Nodes, 0, 0);

    Node *bfsNodes = (Node *) malloc(sizeof(Node) * N);

    size_t *permutation = (size_t *) malloc(sizeof(size_t) * N);

    size_t index = 0;
    uint64_t foundCount = 0;
    int layer = 0;
    while(true) {
        for (size_t i = 0; i < N; i++) {
            if (layers[i] == layer) {
                bfsNodes[index] = Nodes[i];
                permutation[i] = index;
                foundCount++;
                index++;
            }
        }
        layer++;


        if (foundCount == 0) {
            break;
        }
        foundCount = 0;
        continue;
    }

    free(pointer);
    free(layers);

    for (size_t i = 0; i < N; i++) {
        bfsNodes[i].left = permutation[bfsNodes[i].left];
        bfsNodes[i].right = permutation[bfsNodes[i].right];
    }

    free(permutation);

    pointer = bfsNodes;
}

size_t recursivelyInitializeSkewedDfsPointers(Node *nodes, uint64_t indexFrom, uint64_t indexTo, uint64_t from, uint64_t to) {
    if (indexFrom > indexTo) {
        __throw_runtime_error("nothing good!");
    }
    if (indexFrom == indexTo) {
        nodes[indexFrom] = Node(from, 0, 0);
        return indexFrom;
    }
    if (indexTo - indexFrom == 1) {
        nodes[indexFrom] = Node(from, 0, indexTo);
        nodes[indexTo] = Node(to, 0, 0);
        return indexFrom;
    }
    uint64_t middleIndex = (uint64_t) (indexFrom + ((indexTo - indexFrom) * skewedNess));
    if (middleIndex == indexTo && indexTo - indexFrom >= 2) {
        middleIndex = indexTo - 1;
    }
    if (middleIndex == indexFrom&& indexTo - indexFrom >= 2) {
        middleIndex = indexFrom + 1;
    }
    uint64_t middle = (uint64_t) (from + ((to - from) * skewedNess));
    if (middle == to && to - from >= 2) {
        middle = to - 1;
    }
    if (middle == from && to - from >= 2) {
        middle = from + 1;
    }
    Node node(middle, 0, 0);

    node.left = recursivelyInitializeSkewedDfsPointers(nodes, indexFrom + 1, middleIndex, from, middle - 1);
    node.right = recursivelyInitializeSkewedDfsPointers(nodes, middleIndex + 1, indexTo, middle + 1, to);

    nodes[indexFrom] = node;
    return indexFrom;
}

void initializeSkewedDfsPointers() {
    pointer = malloc(sizeof(Node) * N);
    Node *Nodes = (Node *) pointer;

    recursivelyInitializeSkewedDfsPointers(Nodes, 0, N - 1, 1, N);
}

size_t recursivelyInitializeDfsPointers(Node *nodes, uint64_t indexFrom, uint64_t indexTo, uint64_t from, uint64_t to) {
    if (indexFrom > indexTo) {
        __throw_runtime_error("nothing good!");
    }
    if (indexFrom == indexTo) {
        nodes[indexFrom] = Node(from, 0, 0);
        return indexFrom;
    }
    if (indexTo - indexFrom == 1) {
        nodes[indexFrom] = Node(from, 0, indexTo);
        nodes[indexTo] = Node(to, 0, 0);
        return indexFrom;
    }
    uint64_t middleIndex = (indexFrom + indexTo) / 2;
    uint64_t middle = (from + to) / 2;
    Node node(middle, 0, 0);

    node.left = recursivelyInitializeDfsPointers(nodes, indexFrom + 1, middleIndex, from, middle - 1);
    node.right = recursivelyInitializeDfsPointers(nodes, middleIndex + 1, indexTo, middle + 1, to);

    nodes[indexFrom] = node;
    return indexFrom;
}

void initializeDfsPointers() {
    pointer = malloc(sizeof(Node) * N);
    Node *Nodes = (Node *) pointer;

    recursivelyInitializeDfsPointers(Nodes, 0, N - 1, 1, N);
}

void initializeRandomPointers() {
    initializeDfsPointers(); // Could be any, just have to be valid assignment of pointers, starting from the first.

    Node *Nodes = (Node *) pointer;

    size_t *permutation = (size_t *) malloc(sizeof(size_t) * N);

    for (size_t i = 0; i < N; i++) {
        permutation[i] = i;
    }
    for (size_t i = 2; i < N; i++) {
        size_t j = (rand() % (i - 1)) + 1; // Swap with one in index [1, i-1]
        size_t tmp = permutation[i];
        permutation[i] = permutation[j];
        permutation[j] = tmp;

        uint64_t tmpValue = Nodes[i].value;
        Nodes[i].value = Nodes[j].value;
        Nodes[j].value = tmpValue;

        tmp = Nodes[i].left;
        Nodes[i].left = Nodes[j].left;
        Nodes[j].left = tmp;

        tmp = Nodes[i].right;
        Nodes[i].right = Nodes[j].right;
        Nodes[j].right = tmp;
    }

    size_t *inversePermutation = (size_t *) malloc(sizeof(size_t) * N);
    for (size_t i = 0; i < N; i++) {
        inversePermutation[permutation[i]] = i;
    }

    for (size_t i = 0; i < N; i++) {
        Nodes[i].left = inversePermutation[Nodes[i].left];
        Nodes[i].right = inversePermutation[Nodes[i].right];
    }

    free(permutation);
    free(inversePermutation);
}

uint64_t queryPointers(uint64_t query) {
    uint64_t index = 0;
    uint64_t candidate = 0;

    Node *Nodes = (Node *) pointer;

    if (Nodes[index].value > candidate && Nodes[index].value <= query) {
        candidate = Nodes[index].value;
    }

    do {
        if (Nodes[index].value > query) {
            index = Nodes[index].left;
        } else {
            index = Nodes[index].right;
        }

        if (Nodes[index].value > candidate && Nodes[index].value <= query) {
            candidate = Nodes[index].value;
        }
    } while (index != 0); // No negative numbers, so i assume that search trees start at index 1, and nodes with no children point to 0.
    return candidate;
}

uint64_t nearestPowerOfTwo(uint64_t n) {
    uint64_t result = 1;
    while(result < n) {
        result <<= 1;
    }
    return result;
}

void initializeBFSRecursively(uint8_t depth, uint64_t index, uint64_t min, uint64_t max) {
    uint64_t *M = (uint64_t *) pointer;

    uint64_t left_child = (index + 1) * 2 - 1;
    uint64_t right_child = ((index + 1) * 2 + 1) - 1;

    uint64_t pivot = min + (max - min) / 2;

    M[index] = pivot;

    if(left_child < N) {
        initializeBFSRecursively(depth + 1, left_child, min, pivot);
    }
    if(right_child < N) {
        initializeBFSRecursively(depth + 1, right_child, pivot, max);
    }
}

void initializeBfs() {
    pointer = malloc(sizeof(uint64_t) * N);

    initializeBFSRecursively(0, 0, 0, nearestPowerOfTwo(N));
}

void recursivelyInitializeDfs(uint64_t indexFrom, uint64_t indexTo, uint64_t from, uint64_t to) {
    uint64_t *M = (uint64_t *) pointer;
    if (indexFrom > indexTo) {
        throw "Nothing good";
    }
    if (indexFrom == indexTo) {
        M[indexFrom] = from;
        return;
    }
    if (indexTo - indexFrom == 1) {
        M[indexFrom] = from;
        M[indexTo] = to;
        return;
    }
    uint64_t middleIndex = (indexFrom + indexTo) / 2;
    uint64_t middle = (from + to) / 2;
    M[indexFrom] = middle;
    recursivelyInitializeDfs(indexFrom + 1, middleIndex, from, middle - 1);
    recursivelyInitializeDfs(middleIndex + 1, indexTo, middle + 1, to);
}

void initializeDfs() {
    pointer = malloc(sizeof(uint64_t) * N);
    recursivelyInitializeDfs(0, N - 1, 1, N);
}

void recursivelyInitializeDfs2(uint64_t index, uint8_t us, int r, uint64_t min, uint64_t max) {
    uint64_t *M = (uint64_t *) pointer;

    uint64_t pivot = min + (max - min) / 2;
    M[index - 1] = pivot;

    if(us > 0) {
        recursivelyInitializeDfs2(index + 1,                                          us - 1, std::min(r, 1 << us),     min,   pivot);    // left child
        recursivelyInitializeDfs2(index + ((1 << us) - 1) + std::min(r, 1 << us) + 1, us - 1, std::max(r - (1 << us), 0), pivot, max);      // right child
    } else {
        if(r == 0) {
            return;
        }
        if(r == 1) {
            recursivelyInitializeDfs2(index + 1, us, 0, min,   pivot);    // left child
        }
        if(r == 2) {
            recursivelyInitializeDfs2(index + 1, us, 0, min,   pivot);    // left child
            recursivelyInitializeDfs2(index + 2, us, 0, pivot, max);      // right child
        }
    }
}

int DFS2_TOTAL_DEPTH;

// Computes the base 2 logarithm floored.
uint8_t ilog2(uint64_t value) {
    uint8_t depth = -1;
    while (value != 0) {
        value >>= 1, depth++;
    }
    return depth;
}

void initializeDfs2() {
    pointer = malloc(sizeof(uint64_t) * N);
    DFS2_TOTAL_DEPTH = ilog2(N) + 1;

    int r = N - ((1 << (DFS2_TOTAL_DEPTH - 1)) - 1);

    if(r == 1 << (DFS2_TOTAL_DEPTH - 2)) {
        recursivelyInitializeDfs2(1, DFS2_TOTAL_DEPTH - 1, 0, 0, nearestPowerOfTwo(N));
    } else {
        recursivelyInitializeDfs2(1, DFS2_TOTAL_DEPTH - 2, r, 0, nearestPowerOfTwo(N));
    }


}

// pred(x) = max { y \in S | y <= x }

uint64_t queryBfs(uint64_t query) {
    uint64_t *M = (uint64_t *) pointer;
    uint64_t index = 0;
    uint8_t depth = 0;

    uint64_t candidate = 0;

    while (index < N) {
        if (M[index] > candidate && M[index] <= query) {
            candidate = M[index];
        }

        uint64_t left_child = (index + 1) * 2 - 1;
        uint64_t right_child = (((index + 1) * 2) + 1) - 1;

        if (M[index] > query) {
            index = left_child;
        } else {
            index = right_child;
        }

        depth++;
    }
    return candidate;
}

uint64_t queryDfs(uint64_t query) {
    uint64_t *M = (uint64_t *) pointer;
    uint64_t subTreeSize = N / 2;
    uint64_t index = 0;
    uint64_t candidate = 0;

    if (M[index] > candidate && M[index] <= query) {
        candidate = M[index];
    }

    while (subTreeSize > 0) {
        if (M[index] > query) {
            index = index + 1;
        } else {
            index = index + subTreeSize + 1;
        }
        subTreeSize = subTreeSize / 2;

        if (M[index] > candidate && M[index] <= query) {
            candidate = M[index];
        }
    }
    return candidate;
}

uint64_t queryDfs2(uint64_t query) {
    uint64_t* M = (uint64_t*) pointer;

    uint64_t index = 1;

    int r = N - ((1 << (DFS2_TOTAL_DEPTH - 1)) - 1);

    uint8_t start_us;
    if(r == 1 << (DFS2_TOTAL_DEPTH - 2)) {
        start_us = DFS2_TOTAL_DEPTH - 1;
        r = 0;
    } else {
        start_us = DFS2_TOTAL_DEPTH - 2;
    }


    uint64_t candidate = 0;

    if (M[index - 1] > candidate && M[index - 1] <= query) {
        candidate = M[index - 1];
    }

    for(uint8_t us = start_us; us > 0; us--) {
        if (M[index - 1] > query) {
            index = index + 1;                          // Left child
            r = std::min(r, 1 << us);
        } else {
            index = index + ((1 << us) - 1) + std::min(r, 1 << us) + 1;      // Right child
            r = std::max(r - (1 << us), 0);
        }

        if (M[index - 1] > candidate && M[index - 1] <= query) {
            candidate = M[index - 1];
        }
    }

    assert(r >= 0 && r <= 2);

    if(r != 0) {
        if(M[index - 1] > query) {
            index = index + 1;      // Left child

            if (M[index - 1] > candidate && M[index - 1] <= query) {
                candidate = M[index - 1];
            }

        } else if(r == 2) {
            index = index + 2;      // Right child

            if (M[index - 1] > candidate && M[index - 1] <= query) {
                candidate = M[index - 1];
            }
        }
    }

    return candidate;
}

void initializeSortedArray() {
    pointer = malloc(sizeof(uint64_t) * N);
    uint64_t *M = (uint64_t *) pointer;
    for (uint64_t i = 0; i < N; i++) {
        M[i] = i;
    }
}

uint64_t queryBinaryPred(uint64_t query) {
    uint64_t *M = (uint64_t *) pointer;
    uint64_t left = 0;
    uint64_t right = N - 1;
    while (true) {
        uint64_t distance = right - left;
        if (distance == 0) {
            uint64_t point = M[left];
            if (point > query) {
                return M[left - 1];
            } else {
                return M[left];
            }
        } else if (distance == 1) {
            if (M[left] > query) {
                return M[left - 1];
            } else if(M[right] > query) {
                return M[right - 1];
            } else {
                return M[right];
            }
        }
        uint64_t middleIndex = (left + right) / 2;
        uint64_t middle = M[middleIndex];
        if (middle > query) {
            right = middleIndex;
        } else {
            left = middleIndex;
        }

    }
}

#define MULTI_POINTERS 20

uint64_t queryMultiBinaryPred(uint64_t query) {
    uint64_t *M = (uint64_t *) pointer;
    uint64_t left = 0;
    uint64_t right = N - 1;
    while (true) {
        uint64_t distance = right - left;
        if (distance == 0) {
            uint64_t point = M[left];
            if (point > query) {
                return M[left - 1];
            } else {
                return M[left];
            }
        } else if (distance == 1) {
            if (M[left] > query) {
                return M[left - 1];
            } else if(M[right] > query) {
                return M[right - 1];
            } else {
                return M[right];
            }
        } else if (distance <= 3 || distance - 4 < MULTI_POINTERS) {
            uint64_t middleIndex = (left + right) / 2;
            uint64_t middle = M[middleIndex];
            if (middle > query) {
                right = middleIndex;
            } else {
                left = middleIndex;
            }
        } else {
            int sum = 0;
            for (int i = 0; i < MULTI_POINTERS; i++) {
                size_t markerIndex = (left + 1) + ((distance - 2) / (MULTI_POINTERS + 1)) * (i + 1);
                if (M[markerIndex] > query) {
                    sum += 1;
                }
            }

            sum = MULTI_POINTERS - sum;

            uint64_t newLeft = (left) + ((distance - 2) / (MULTI_POINTERS + 1)) * (sum);
            uint64_t newRight = (left + 1) + ((distance - 2) / (MULTI_POINTERS + 1)) * (sum + 1);
            if (sum == 0) {
                newLeft = left;
            } else if (sum == MULTI_POINTERS) {
                newRight = right;
            }

            right = newRight;
            left = newLeft;
        }
    }
}

uint64_t queryMultiBinaryPredEarlyExit(uint64_t query) {
    uint64_t *M = (uint64_t *) pointer;
    uint64_t left = 0;
    uint64_t right = N - 1;
    while (true) {
        uint64_t distance = right - left;
        if (distance == 0) {
            uint64_t point = M[left];
            if (point > query) {
                return M[left - 1];
            } else {
                return M[left];
            }
        } else if (distance == 1) {
            if (M[left] > query) {
                return M[left - 1];
            } else if(M[right] > query) {
                return M[right - 1];
            } else {
                return M[right];
            }
        } else if (distance <= 3 || distance - 4 < MULTI_POINTERS) {
            uint64_t middleIndex = (left + right) / 2;
            uint64_t middle = M[middleIndex];
            if (middle > query) {
                right = middleIndex;
            } else {
                left = middleIndex;
            }
        } else {
            int sum = 0;
            uint64_t newLeft = left;
            bool exited = false;
            for (int i = 0; i < MULTI_POINTERS; i++) {
                size_t markerIndex = (left + 1) + ((distance - 2) / (MULTI_POINTERS + 1)) * (i + 1);
                if (M[markerIndex] > query) {
                    if (i == 0) {
                        right = markerIndex;
                    } else {
                        left = newLeft;
                        right = markerIndex;
                    }
                    exited = true;
                    break;
                } else {
                    newLeft = markerIndex;
                }
            }
            if (!exited) {
                left = newLeft;
            }
            // newRight = right;
        }
    }
}


uint64_t queryLinearPred(uint64_t query) {
    uint64_t *M = (uint64_t *) pointer;
    for (int i = 0; i < N; i++) {
        uint64_t point = M[i];
        if (point > query) {
            if (i == 0) {
                return M[0];
            } else {
                return M[i - 1];
            }
        }
    }
    return M[N-1];
}
